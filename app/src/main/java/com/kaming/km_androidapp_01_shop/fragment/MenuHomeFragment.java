package com.kaming.km_androidapp_01_shop.fragment;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kaming.km_androidapp_01_shop.R;

public class MenuHomeFragment extends BaseFragment implements View.OnClickListener {

             private final static String TAG = MenuHomeFragment.class.getSimpleName();

             private TextView tv_search_home;
             private TextView tv_message_home;
             private RecyclerView rv_home;
             private ImageView ib_top;

             @Override
     public View initView() {
                 Log.e(TAG,"主页面的Fragment的UI被初始化了");
                 View view = View.inflate(mContext,R.layout.fragment_home_menu,null);
                 //初始化布局控件
                 tv_search_home = (TextView) view.findViewById(R.id.menu_home_title_search);
                 tv_message_home = (TextView) view.findViewById(R.id.menu_home_titlebar_scan);
                 rv_home = (RecyclerView) view.findViewById(R.id.rv_home);
                 ib_top = (ImageView) view.findViewById(R.id.ib_top);

                 //设置点击事件
                 ib_top.setOnClickListener(this);
                 tv_search_home.setOnClickListener(this);
                 tv_message_home.setOnClickListener(this);
                 return view;
             }


             
             @Override
     public void initData() {
                 super.initData();
                 Log.e(TAG,"主页面的Fragment的数据被初始化了");
             }

             @Override
     public void onClick(View view) {
                 switch (view.getId()){
                     case R.id.ib_top: //置顶的监听
                         rv_home.scrollToPosition(0);
                         break;
                     case R.id.menu_home_title_search:  //搜索的监听
                         Toast.makeText(mContext,"搜索",Toast.LENGTH_SHORT).show();
                         break;
                     case R.id.menu_home_titlebar_scan: //消息监听
                         Toast.makeText(mContext,"进入消息中心",Toast.LENGTH_SHORT).show();
                         break;
                 }
             }
}
